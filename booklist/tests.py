from django.test import LiveServerTestCase
from django.test import Client
from selenium import webdriver

# Place app imports here.

# Create your tests here.
class BookListTest(LiveServerTestCase):

	# setting up the test
	def setUp(self):
		port = 8000
		self.browser = webdriver.Firefox()
		self.client = Client().get("http://localhost:8000/booklist")
		self.browser.get("http://localhost:8000/booklist")
	
	# test if server exists
	def test00ServerExists(self):
		self.assertEqual(self.client.status_code, 200)
		
	# test if the correct page is displayed
	def test01CorrectPageDisplayed(self):
		self.assertTemplateUsed(self.client, "booklist.html")

