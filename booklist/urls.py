from django.urls import path
from .views import booklist

urlpatterns = [
    path('', booklist, name = 'booklist'),
]
