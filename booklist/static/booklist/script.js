$(document).ready(function() {
	var fav = 0;
	var xmlHttp = new XMLHttpRequest();
	var table = $("table");
	// load JSON
	xmlHttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var data = JSON.parse(this.responseText);
			var items = data.items;
			var out = "";

			for (var i = 0; i < items.length; i++) {
				var info = items.i.volumeInfo;
				out += "<td>" + info.thumbnail
					+ "</td><td>" + info.title
					+ "</td><td>" + info.authors
					+ "</td><td>" + info.publisher
					+ "</td><td>" + info.publishedDate
					+ "</td><tr>";
			}
			table.append(out);
		}
		xmlHttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=quilting", true);
		xmlHttp.send();
	};

	// function for changing the button appearance and adding the favorites list	
	$("#fav-button").click(function() {
		/*
		if () {
			fav++;
			$("#fav-button").text("&#9733; Added");
		};
		else {
			fav--;
			$("#fav-button").text("&#9733; Add To Favorites");
		}
		*/
		$("#fav-button").toggleClass("selected");
		$("#fav-text").text("Favorites &#9733; : " + fav);	
	});
});
