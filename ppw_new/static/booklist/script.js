$(document).ready(function() {
	var fav = 0;

	// load JSON
	$.getJSON("https://www.googleapis.com/books/v1/volumes?q=quilting", function(data) {
		var items = [];
		$.each(data, function(key : val) {
			items.push("<tr><td id='" + key + "'>" + val + "</td></tr>");
		});
		$("table", {
			html : items.join(""),
		}).appendTo("table");
	});

	// function for changing the button appearance and adding the favorites list	
	$("#fav-button").click(function() {
		/*
		if () {
			fav++;
			$("#fav-button").text("&#9733; Added");
		};
		else {
			fav--;
			$("#fav-button").text("&#9733; Add To Favorites");
		}
		*/
		$("#fav-button").toggleClass("selected");
		$("#fav-text").text("Favorites &#9733; : " + fav);	
	});
});
