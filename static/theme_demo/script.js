$(document).ready(function() {
	// Default variables
	var themeChanger = $("#theme-changer");
	var profileDesc = $("#profile-desc");
	var text = $("h1, h2, h3, h4, p");
	var bg = $("div");
	var header = $(".header");
	var input = $("input[type='text'], input[type='email'], input[type='password']");
	var button = $("input[type='submit'], button");
		
	// Button variables
	var mainTheme = $("#default");
	var astro = $("#astro");
	var sakura = $("#sakura");
	var submit = $("#subscribe");
	// Theme changer accordion
	themeChanger.accordion( {
		collapsible: true,
		active: false,
		icons: false,
		classes: {
			"ui-accordion" : "ui-accordion theme-changer",
			"ui-accordion-content" : "ui-accordion-content theme-changer-content"
		}
	});
	
	// Main accordion
	profileDesc.accordion( {
		collapsible: false,
		active: 0,
		icons: false,
		classes: {
			"ui-accordion" : "ui-accordion profile-desc",
			"ui-accordion-content" : "ui-accordion-content profile-desc"
		}
	});
	
	// Theme enabler and disabler
	function enable(main, secondary, active) {
		text.addClass(main);
		bg.addClass(main);
		button.addClass(secondary);
		input.addClass(secondary);
	}
	
	function disable(main, secondary, active) {
		text.removeClass(main);
		bg.removeClass(main);
		button.removeClass(secondary);
		input.removeClass(secondary);
	}
		
	// Theme changer functions
	mainTheme.click(function() {
		disable("sakura", "sakura-secondary", "sakura-active");
		disable("astro", "astro-secondary", "astro-active");
	});
	
	astro.click(function() {
		disable("sakura", "sakura-secondary", "sakura-active");
		enable("astro", "astro-secondary", "astro-active");
	});
	
	sakura.click(function() {
		enable("sakura", "sakura-secondary", "sakura-active");
		disable("astro", "astro-secondary", "astro-active");
	});
	
	// Data submission function
	submit.click(function() {
		$.ajax(
});
