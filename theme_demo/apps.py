from django.apps import AppConfig


class ThemeDemoConfig(AppConfig):
    name = 'theme_demo'
