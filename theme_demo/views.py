from django.shortcuts import render
from .models import Content
from .models import Subscriber
from .forms import SubscribeForm

# Create your views here.

def display(request):
	
	#activities = Content.objects.create(
	s1_title = "Activities"
	s1_desc = "There are currently no active activities."
	#)
	
	#achievements = Content.objects.create(
	s3_title = "Achievements"
	s3_desc = "- Participant of National Science Olympiad, Computer Science category, province level (2015)"
	#)
	
	#orgExperiences = Content.objects.create(
	s2_title = "Organization Experiences"
	s2_desc = "- Head of Smanda Interactive Club, Game Development and Internet division (2015 - 2016)"
	#)

	form = SubscribeForm(request.POST)
	manager = Subscriber.objects
	
	if form.is_valid():
		nameInsert = form.cleaned_data["name"]
		emailInsert = form.cleaned_data["email"]
		passInsert = form.cleaned_data["password"]
		
		sub = manager.create(name=nameInsert, email=emailInsert, password=passInsert)
	
	response = {
		"s1_title" : s1_title,
		"s1_desc" : s1_desc,
		"s2_title" : s2_title,
		"s2_desc" : s2_desc,
		"s3_title" : s3_title,
		"s3_desc" : s3_desc,
		"form" : form
	}
	
	return render(request, "theme_demo.html", response)


