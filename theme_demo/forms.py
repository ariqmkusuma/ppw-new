from django import forms

class SubscribeForm(forms.Form):

	name = forms.CharField(max_length=180)
	email = forms.EmailField()
	password = forms.CharField(min_length=8, max_length=32, widget=forms.PasswordInput)
