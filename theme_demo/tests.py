from django.test import LiveServerTestCase
from django.test import Client
from selenium import webdriver
import time

# Place app imports here.
from .models import Content

# Create your tests here.
class ThemeDemoTest(LiveServerTestCase):

	port = 8081

	# setting up the test
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.client = Client().get("http://localhost:8081/theme_demo/")
		self.browser.get("http://localhost:8081/theme_demo/")
	
	# test if server exists
	def test00ServerExists(self):
		self.assertEqual(self.client.status_code, 200)
		
	# test if the correct page is displayed
	def test01CorrectPageDisplayed(self):
		self.assertTemplateUsed(self.client, "theme_demo.html")
	
	# test if themes exist
	def test02ThemesExist(self):
		htmlChecker = self.browser.content.decode("utf8")
		self.assertIn("theme_demo/main.css", htmlChecker)
		self.assertIn("theme_demo/astro.css", htmlChecker)
		self.assertIn("theme_demo/sakura.css", htmlChecker)

	# test if theme changer exists
	def test03ThemeChangerExists(self):
		themeChanger = self.browser.find_element_by_id("theme-changer")
		self.assertIsNotNone(themeChanger)
	
	# test if theme changer is functional
	def test04ThemeChangerFunctional(self):
		header = self.browser.find_element_by_name("header")
		themeChanger = self.browser.find_element_by_id("theme-changer")
		themeChanger.click()
		sakura = self.browser.find_element_by_id("sakura")
		sakura.click()
				
	# test if data can exist in database
	def test05DataExists(self):
		data = Content.objects.create(title = "Data input test")
		result = data.getTitle()
		self.assertEqual(result, "Data input test")
		
	# test if data is displayed
	def test06DataDisplayed(self):
		htmlChecker = self.browser.content.decode("utf8")
		self.assertIn("Data input test", htmlChecker)

	# end the test
	def tearDown(self):
		self.browser.quit()

