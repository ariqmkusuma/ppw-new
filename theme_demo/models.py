from django.db import models

# Create your models here.

class Content(models.Model):

	title = models.CharField(max_length = 60)
	desc = models.TextField()
	
class Subscriber(models.Model):

	name = models.CharField(max_length = 180)
	email = models.EmailField()
	password = models.CharField(max_length = 32)

