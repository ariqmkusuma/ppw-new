from django.test import TestCase
from django.test import Client
from .models import Status
from .views import display
from selenium import webdriver
import time

# Create your tests here.

class DataTest(TestCase):
	
	# initialize test
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.client = Client()
		self.server = self.client.get("http://localhost:8000/")
		self.browser.get("http://localhost:8000/")

	# checks if server exists
	def test00ServerExists(self):
		self.assertEqual(self.server.status_code, 200)

	# check if the correct page is received
	def test01PageExists(self):
		self.assertTemplateUsed(self.server, "status.html")
		self.assertIn("Status", self.browser.title)
	
	# check if the form exists		
	def test02FormExists(self):
		formConf = self.browser.find_element_by_id("id_msg")
		submitConf = self.browser.find_element_by_name("submit-button")
		self.assertIsNotNone(formConf)
		self.assertIsNotNone(submitConf)
	
	# check if status exists in database
	def test03StatusExists(self):
		status = Status.objects.create(msg = "Direct model input test")
		status.save()
		
		self.assertEqual(1, Status.objects.all().count())
	
	# check if the form can be used
	def test04FormAbleToPost(self):
		time.sleep(2)
		formConf = self.browser.find_element_by_id("id_msg")
		submitConf = self.browser.find_element_by_name("submit-button")
		formConf.send_keys("Post input test")
		submitConf.click()

		time.sleep(2)		
		self.assertEqual(2, Status.objects.all().count())
	
	# check if status is displayed on screen
	def test05StatusDisplayed(self):
		time.sleep(2)
		displayedStatus = self.browser.content.decode("utf8")
		self.assertIn("Post input test", displayedStatus)
		self.assertIn("Direct model input test", displayedStatus)

	# the full coba-coba test
	def test06CobaCoba(self):
		time.sleep(2)
		formConf = self.browser.find_element_by_id("id_msg")
		submitConf = self.browser.find_element_by_name("submit-button")
		formConf.send_keys("Coba coba")
		submitConf.click()
		
		time.sleep(2)
		displayedStatus = self.browser.content.decode("utf8")
		self.assertIn("Coba coba", displayedStatus)
	
	# end the test
	def tearDown(self):
		self.browser.quit()
		
