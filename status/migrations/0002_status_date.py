# Generated by Django 2.1.1 on 2018-10-11 05:06

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('status', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='date',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
