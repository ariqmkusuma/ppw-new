from django.shortcuts import render
from .models import Status
from .forms import StatusForm

# Create your views here.

def display(request):
    form = StatusForm(request.POST)
    manager = Status.objects
    
    if form.is_valid():
    	msgIn = form.cleaned_data["msg"]
    	
    	stat = manager.create(msg = msgIn)
    	stat.save()
    
    response = {
    	"status_form" : form,
    	"status_display" : display,
    }
    
    return render(request, "status.html", response)
