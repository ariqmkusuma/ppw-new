from django.test import TestCase
from django.test import Client
from .views import index
from selenium import webdriver

# Create your tests here.

class ProfileTest(TestCase):

	# initialize test
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.client = Client()
		self.server = self.client.get("http://localhost:8000/profile/")

	# checks if server exists
	def testServerExists(self):
		self.assertEqual(self.server.status_code, 200)

	# check if the correct page is received
	def testPageExists(self):
		self.assertTemplateUsed(self.server, "index.html")

	# end the test
	def tearDown(self):
		self.browser.quit()
