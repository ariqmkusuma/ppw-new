from django.shortcuts import render

# Create your views here.
email = "ariqmkusuma@gmail.com"
address = "Mutiara Sentosa Boulevard Blok D/2 Sawangan, Depok, Indonesia"
education = "- High School, SMAN 2 Cirebon (2014 - 2017)\n- Undergraduate, Computer Science, Universitas Indonesia (2017 - )"
experiences = "- Head of the 4th Division (Gaming and Internet) of Smanda Interactive Club (SMAN 2 Cirebon, 2015 - 2016)\n- Freelance Designer & Sketch Artist (2014 - )\n- Hobbyist Game Developer (2015 - )\n- Hobbyist 3D Artist (2012 - )"

def index(request):
	response = {'email' : email,
				'address' : address,
				'education' : education,
				'experiences' : experiences
				}
	return render(request, 'index.html', response)


